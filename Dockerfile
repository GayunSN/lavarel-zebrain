FROM ubuntu:latest

# Set correct environment variables
ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive

# Use Supervisor to run and manage all other services
CMD ["/usr/local/bin/supervisord", "-c", "/etc/supervisord.conf"]

# Install required packages
RUN	apt-get update && apt-get install -y \
		curl \
		libcurl4 \
		libcurl4-openssl-dev \
		python \
		cron \
		nano \
		nginx \
		php7.4-fpm \
		php7.4-cli \
		php7.4-gd \
#		php7.4-mcrypt \
		php7.4-sqlite \
		php7.4-curl \
		php7.4-opcache \
		php7.4-mbstring \
		php7.4-zip \
		php7.4-xml \
		php-mysql \
		redis-server \
		nodejs \
		npm && \
	mkdir -p /share/public/ && \
	mkdir -p /etc/supervisord/ && \
	mkdir /var/log/supervisord && \
	curl https://bootstrap.pypa.io/ez_setup.py -o - | python && \
	easy_install supervisor

# Copy supervisor config files, Laravel cron file, & default site configuration
COPY provision/conf/supervisor.conf /etc/supervisord.conf
COPY provision/service/* /etc/supervisord/
COPY provision/cron/laravel /etc/cron.d/laravel
COPY provision/conf/nginx-default /etc/nginx/sites-available/default
COPY provision/public /share/public

# Configure PHP, Nginx, Redis, and clean up
RUN sed -i 's/;opcache.enable=0/opcache.enable=1/g' /etc/php/7.4/fpm/php.ini && \
	sed -i 's/;opcache.fast_shutdown=0/opcache.fast_shutdown=1/g' /etc/php/7.4/fpm/php.ini && \
	sed -i 's/;opcache.enable_file_override=0/opcache.enable_file_override=1/g' /etc/php/7.4/fpm/php.ini && \
	sed -i 's/;opcache.revalidate_path=0/opcache.revalidate_path=1/g' /etc/php/7.4/fpm/php.ini && \
	sed -i 's/;opcache.save_comments=1/opcache.save_comments=1/g' /etc/php/7.4/fpm/php.ini && \
	sed -i 's/;opcache.revalidate_freq=2/opcache.revalidate_freq=60/g' /etc/php/7.4/fpm/php.ini && \
	sed -i 's/pm.max_children = 5/pm.max_children = 12/g' /etc/php/7.4/fpm/pool.d/www.conf && \
	sed -i 's/pm.start_servers = 2/pm.start_servers = 4/g' /etc/php/7.4/fpm/pool.d/www.conf && \
	sed -i 's/pm.min_spare_servers = 1/pm.min_spare_servers = 4/g' /etc/php/7.4/fpm/pool.d/www.conf && \
	sed -i 's/pm.max_spare_servers = 3/pm.max_spare_servers = 8/g' /etc/php/7.4/fpm/pool.d/www.conf && \
	echo "daemon off;" >> /etc/nginx/nginx.conf && \
	sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.4/fpm/php-fpm.conf && \
	sed -i 's/^daemonize yes/daemonize no/' /etc/redis/redis.conf && \
	sed -i 's/^# maxmemory <bytes>/maxmemory 32mb/' /etc/redis/redis.conf && \
	sed -i 's/^# maxmemory-policy volatile-lru/maxmemory-policy allkeys-lru/' /etc/redis/redis.conf && \
	chmod 644 /etc/cron.d/laravel && \
#	ln -s /usr/bin/nodejs /usr/bin/node && \
	apt-get -yq autoremove --purge && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
EXPOSE 80